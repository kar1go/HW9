<?php


namespace App\DB;

use PDO;

/**
 * Class DBConnector
 * @package App\DB
 */
class DBConnector
{

    private const DB_NAME = 'docker';
    private const DB_HOST = 'mysql';
    private const DB_USER = 'root';
    private const DB_PASSWORD = 'secret';

    /**
     * @var ?PDO
     */
    private static ?PDO $instance = null;

    /**
     * @return PDO
     */
    public static function getInstance(): PDO
    {
        if (!self::$instance) {
            $instance = new PDO(
                sprintf('mysql:dbname=%s;host=%s',
                    self::DB_NAME,
                    self::DB_HOST
                ),
                self::DB_USER,
                self::DB_PASSWORD
            );
            $instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            self::$instance = $instance;
        }

        return self::$instance;
    }
}