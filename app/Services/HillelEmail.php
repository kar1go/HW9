<?php

namespace App\Services;


use App\Exceptions\HillelEmailException;

class HillelEmail extends Email
{
    public function __construct($value)
    {
        parent::__construct($value);

        if (strpos($value, 'hillel.ua') === false) {
            throw new HillelEmailException();
        }
    }

    public function getValue()
    {

    }
}